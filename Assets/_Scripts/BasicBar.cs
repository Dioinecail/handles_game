﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BasicBar : OutputRecieverBase
{
    public bool IsEmpty { get; set; }

    public Image slider;
    public float sliderChangeRate;
    public float inputMultiplier;

    public UnityEvent onSliderEmptyEvent;
    public UnityEvent onSliderFullEvent;

    void Update()
    {
        if (IsEmpty)
            return;

        ChangeSliderValue();
    }

    public void ChangeSliderValue()
    {
        slider.fillAmount += sliderChangeRate * Time.deltaTime;
        if(slider.fillAmount <= 0)
            onSliderEmptyEvent?.Invoke();
    }

    public void OnRecieveInput(float input)
    {
        slider.fillAmount += input * inputMultiplier;

        if (slider.fillAmount >= 1)
            onSliderFullEvent?.Invoke();
    }

    private void OnEnable()
    {
        sender.onAngleChanged += OnRecieveInput;
    }

    private void OnDisable()
    {
        sender.onAngleChanged -= OnRecieveInput;
    }
}