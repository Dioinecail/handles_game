﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public delegate void OnHandleAngleChanged(float output);

public class RotatableHandle : OutputSenderBase, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private Camera mainCam;

    private Vector2 beginDragPosition;
    private Vector2 dragPosition;
    private Vector2 endDragPosition;

    private float beginDragAngle;

    private float dragAngle;
    private float lastDragAngle;
    private float deltaAngle;

    private float endDragAngle;

    private float currentObjectAngle;

    void Awake()
    {
        mainCam = Camera.main;
    }

    private float CalculateAngleToPoint(Vector2 point)
    {
        Vector2 direction = point - (Vector2)transform.position;

        float angle = Vector2.Angle(Vector2.up, direction);

        Vector3 cross = Vector3.Cross(Vector2.up, direction);

        if (cross.z > 0)
            angle = -angle;

        return angle;
    }

    private void RotateObject(float angle)
    {
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);

        transform.rotation *= targetRotation;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        beginDragPosition = mainCam.ScreenToWorldPoint(eventData.position);
        beginDragAngle = CalculateAngleToPoint(beginDragPosition);

        // Precalculate position and angle so that the handler
        // will not jump at the start of dragging
        dragPosition = mainCam.ScreenToWorldPoint(eventData.position);
        dragAngle = CalculateAngleToPoint(dragPosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        lastDragAngle = dragAngle;

        dragPosition = mainCam.ScreenToWorldPoint(eventData.position);
        dragAngle = CalculateAngleToPoint(dragPosition);

        deltaAngle = dragAngle - lastDragAngle;

        if (Mathf.Abs(deltaAngle) > 180)
            return;

        RotateObject(-deltaAngle);

        OnOutputChanged(deltaAngle);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        endDragPosition = mainCam.ScreenToWorldPoint(eventData.position);
        endDragAngle = CalculateAngleToPoint(endDragPosition);

        currentObjectAngle = transform.eulerAngles.z;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(dragPosition, 0.1f);
    }
}