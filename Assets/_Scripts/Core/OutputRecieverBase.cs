﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public delegate void OnRecievedInput(float input);

public class OutputRecieverBase : MonoBehaviour, IOutputHandler
{
    public OutputSenderBase sender;

    public OnRecievedFloatInputEvent onRecievedFloatInput;
    public OnRecievedStringInputEvent onRecievedStringInput;
    private float receivedInput;

    public void OnRecievedInput(float input)
    {
        receivedInput = input;
        onRecievedFloatInput?.Invoke(input);
        onRecievedStringInput?.Invoke(input.ToString("0.0"));
    }

    private void OnEnable()
    {
        sender.onAngleChanged += OnRecievedInput;
    }

    private void OnDisable()
    {
        sender.onAngleChanged -= OnRecievedInput;
    }
}

public interface IOutputHandler
{
    void OnRecievedInput(float input);
}

[System.Serializable]
public class OnRecievedFloatInputEvent : UnityEvent<float> { }
[System.Serializable]
public class OnRecievedStringInputEvent : UnityEvent<string> { }