﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputSenderBase : MonoBehaviour, IOutputSender
{
    public event OnHandleAngleChanged onAngleChanged;

    public void OnOutputChanged(float output)
    {
        onAngleChanged?.Invoke(output);
    }
}

public interface IOutputSender
{
    event OnHandleAngleChanged onAngleChanged;

    void OnOutputChanged(float output);
}